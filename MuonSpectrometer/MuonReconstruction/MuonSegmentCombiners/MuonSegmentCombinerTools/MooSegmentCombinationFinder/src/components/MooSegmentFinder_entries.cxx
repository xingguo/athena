/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/
#include "../MooSegmentCombinationFinder.h"
#include "../MooSegmentFinderAlg.h"
#include "../MuonSegmentFinderAlg.h"
#include "../MuonSegmentFilterAlg.h"


DECLARE_COMPONENT(MooSegmentFinderAlg)
DECLARE_COMPONENT(MuonSegmentFinderAlg)
DECLARE_COMPONENT(MuonSegmentFilterAlg)
DECLARE_COMPONENT(Muon::MooSegmentCombinationFinder)
